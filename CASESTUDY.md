#Price Case Study
##### Aggregations 
Add pricing to aggregations in Search/config/search.php, I will use the ranges from published dates as inspiration.
```php
    [
        'title'    => 'Price',
        'field'    => (function(){
            $country = app(SearchTerms::class)->get('country', 'GB');
            return 'prices.'.$country.'.price';
        })(),
        'name'    => 'price[]',
        'type'    => 'radio',
        'ranges' => [
            [
                'key' => 'Below 10',
                'from' => 0.00,
                'to' => 10.00
            ],
            [
                'key'  => '10 to 20',
                'from'   => 10.00,
                'to' => 20.00
            ],
            [
                'key'  => '20 to 30',
                'from'   => 20.00,
                'to' => 30.00
            ],
            [
                'key' => '30 to 50',
                'from' => 30.00,
                'to' => 50.00
            ],
            [
                'key' => 'Over 50',
                'from' => 50.00
            ]
        ]
    ]
```
_Note: that price is a nested object._

##### Parameters
in `app\config\search.php` I added 

```php
    [
        ....,
        'price'
    ]
```
and in `app/Http/Requests/SearchTerms.php` in the function `rules()`
```php
[
    ...               => ...,
    'price'           => 'sometimes|array',
    'price.*'         => 'sometimes|string',
]
```
and in function `translated()`
```php
switch(...) {
    case 'price':
        if (!is_array($value)) {
            $value = [$value];
        }
        $price_groups = collect($value)->map(function($price){
            if (preg_match('/^(\d+)?\s?(\w+)\s(\d+)$/', $price, $matched)) {
                list($orig, $gte, $connection, $lte) = $matched;
                if ($connection) {
                    switch (strtolower($connection)) {
                        case 'below':
                            return [
                                'gte' => 0,
                                'lte' => $lte
                            ];
                            break;
                        case 'over':
                        case 'from':
                            return [
                                'gte' => $lte
                            ];
                        case 'to':
                        default:
                            return [
                                'gte' => $gte,
                                'lte' => $lte
                            ];
                    }
                }
            }
        });
    
        $field = function(){
            return 'prices.'.strtoupper($this->route('countryCode') ?? 'GB').'.price';
        };
    
        if ($price_groups->count() == 1) {
            return [$key => [
                'range' => [
                    $field() => $price_groups->flatMap(function ($element) {
                        return $element;
                    })->toArray()
                ]
            ]];
        } else {
            return [$key => [
                'should' => $price_groups->map(function ($element) use ($field) {
    
                    return ['range' => [$field() => $element]];
                })->toArray()
            ]];
        }
        break;
```
and if you want, add something in unit testing like this
```php
    /** @test */
    public function use_the_price_in_translate_function()
    {
        $parameters = $this->app->make(SearchTerms::class);

        $parameters->replace([
            'price'          => ['30 to 50']
        ]);

        $parameters->validateResolved();

        $response = $parameters->translated();

        $this->assertEquals([
            "price" => [
                "range" => [
                    "prices.GB.price" => [
                        "gte" => "30",
                        "lte" => "50"
                    ]
                ]
            ]
        ], $response->toArray(), 'price is not parsed correctly in SearchTerms::translated');
    }
```