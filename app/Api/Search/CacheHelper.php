<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 23/08/2018
 * Time: 11:28
 */

namespace App\Api\Search;


class CacheHelper
{
    public static function key(array $terms = [])
    {
        return collect($terms)->sortKeys()->toJson();
    }
}