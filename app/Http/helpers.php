<?php
/**
 * Created by PhpStorm.
 * User: seb
 * Date: 24/08/2018
 * Time: 13:28
 */

function betterUcwords(string $sentence): string
{
    preg_match_all('/\w+/u', $sentence, $words);
    preg_match_all('/\W+/u', $sentence, $spaces);

    $words = array_first($words);
    $spaces = array_first($spaces);

    $return = '';
    for ($n = 0; $n <= count($words); $n++) {
        if (isset($words[$n])) {
            switch (strtolower($words[$n])) {
                case 'us':
                case 'usa':
                    $word = strtoupper($words[$n]);
                    break;
                default:
                    if (preg_match('/^(mc|mac)(\w)(\w+)/i', $words[$n], $found)) {
                        $word = ucfirst($found[1]) . strtoupper($found[2]) . $found[3];
                    } elseif ($words[$n] == 's' && isset($spaces[$n-1]) && $spaces[$n-1] == "'") {
                        $word = $words[$n];
                    } else {
                        $word = ucwords($words[$n]);
                    }
                    break;
            }
            $return .= $word . (isset($spaces[$n]) ? $spaces[$n] : '');
        }
    }

    return $return;
}